<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Macadamia_Challenge
 * @since 1.0
 * @version 1.2
 */

?>
		</div><!-- #content -->
		<footer id="colophon" class="" role="contentinfo">
			<div class="container">
				<div class="row partner">

					<div class="col-md-6"><a class="btn-image " href="http://www.australian-macadamias.org/consumer/" target="_blank"><img src="https://s3-ap-southeast-2.amazonaws.com/peazie-syd/uploads/company-1/campaign-3604/campaign_app-3853/campaign_app_page-11885/element-55420/59dacd50346b05-48603199.jpg" class="image-responsive"></a></div>
					<div class="col-md-6"><img src="https://s3-ap-southeast-2.amazonaws.com/peazie-syd/uploads/company-1/campaign-3604/campaign_app-3853/campaign_app_page-11885/element-55422/59dacd44453101-14107644.jpg" class="img-responsive"></div>
				</div>
			</div>
			<div class="menu-section">
				<div class="container">
					<?php dynamic_sidebar( 'footer' ); ?>
				</div>
			</div>
		</footer>
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_directory') ?>/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory') ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>
