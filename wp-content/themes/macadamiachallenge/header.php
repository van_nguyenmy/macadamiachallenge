<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Macadamia_Challenge
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
<link href="<?php echo get_bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo get_bloginfo('template_url'); ?>/assets/css/main.css" rel="stylesheet" type="text/css" media="screen" />	
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="" role="banner">
		<?php if ( has_nav_menu( 'top' ) ) : ?>
			<div class="menu-section">
				<div class="container">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
			</div>
		<div class="container">
			<div class="language">
				<div class="dropdown ">
					<button class="btn btn-language dropdown-toggle" type="button" data-toggle="dropdown">English<span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li><a href="#">English</a></li>
						<li><a href="#">Japanese</a></li>
						<li><a href="#">Chinese</a></li>
					</ul>
				</div>
			</div>	
		</div>
		<?php endif; ?>
		<div class="banner"><img src="<?php echo the_field('banner') ?>"></div>
	</header>