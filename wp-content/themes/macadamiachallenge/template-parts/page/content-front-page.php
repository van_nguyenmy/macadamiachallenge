<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Macadamia_Challenge
 * @since 1.0
 * @version 1.0
 */

?>
<div id="post-<?php the_ID(); ?>">	
		<?php	the_content(); ?>
</div>