<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Macadamia_Challenge
 * @since 1.0
 * @version 1.0
 */

?>

<div class="container" id="post-<?php the_ID(); ?>">
		<?php	the_content(); ?>
</div>
