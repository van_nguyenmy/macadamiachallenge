<div id="post-<?php the_ID(); ?>">
<div class="prize">
	<div class="block1 block bg-light">
		<h1><img class="img-responsive" title="Entry Categories" src="/wp-content/themes/macadamiachallenge/assets/images/tittle_1_prize.png" /></h1>
		<p class="marginT20 txt">You can enter in any one of the four categories listed below, and will need to tell us if your creation has a Western or Asian flavour profile. Learn more about these profiles here. </p>
		<p class="marginT20 txt">All creations should be developed with ‘on the go’ consumption in mind. </p>
	</div>	
	<div class="block4 block packet-prize">
		<div class="wrap categories">
			<div class="text-center title-cat">Categories</div>
			<h5>Confectionary</h5>
			<p>This includes sweets and chocolates with either a Western or Asian flavour profile. </p>
			<h5>Cereals</h5>
			<p>This includes cereals with either a Western or Asian flavour profile. </p>
			<h5>Snacks and snack mixes</h5>
			<p>This includes sweet, savoury or healthy snack and snack mixes, balls and cereal or other bars with either a Western or Asian flavour profile. </p>	
			<h5>Future foods</h5>
			<p>This blue sky category includes visionary food products that are new to market with either a Western or Asian flavour profile. </p>
		</div>	
	</div>
	<div class="category-gallery bg-light">
		<h1><img class="img-responsive" title="for inspiration" src="/wp-content/themes/macadamiachallenge/assets/images/tittle_category.png" /></h1>
		<div class="row">
			<div class="col-md-12">
						<div class="well"> 
									<div id="myCarousel" class="carousel slide">

									<ol class="carousel-indicators">
											<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											<li data-target="#myCarousel" data-slide-to="1"></li>
									</ol>

									<!-- Carousel items -->
									<div class="carousel-inner">

									<div class="item active">
										<div class="row">
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
										</div><!--/row-fluid-->
									</div>
									<div class="item">
										<div class="row">
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
											<div class="col-md-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
										</div><!--/row-fluid-->
									</div>

									</div><!--/carousel-inner-->

									<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
									<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
									</div><!--/myCarousel-->

							</div><!--/well-->   
		</div>
</div>
	</div>
	<div class="text-center marginT30 block5"><a class="btn btn-enter" href="/enter">Enter Now</a></div></div>
<!--		<?php	the_content(); ?>-->
</div>