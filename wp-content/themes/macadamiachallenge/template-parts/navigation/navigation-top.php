<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Macadamia_Challenge
 * @since 1.0
 * @version 1.2
 */

?>
	<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'macadamiachallenge' ); ?>">
		<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false" style="display:none">
			<?php
			echo macadamiachallenge_get_svg( array( 'icon' => 'bars' ) );
			echo macadamiachallenge_get_svg( array( 'icon' => 'close' ) );
			_e( 'Menu', 'macadamiachallenge' );
			?>
		</button>

		<?php wp_nav_menu( array(
			'theme_location' => 'top',
			'menu_id'        => 'top-menu',
		) ); ?>
	</nav>

